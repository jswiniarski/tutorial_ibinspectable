//
//  AppHelper.swift
//  PersonDownloader
//
//  Created by Jerzyk on 7/15/16.
//  Copyright © 2016 JS. All rights reserved.
//

import Foundation

func DLog(message: String, function: String = #function) {
    print("\(function): \(message)")
    
}