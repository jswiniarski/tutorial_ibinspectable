//
//  ViewController.swift
//  JSTest
//
//  Created by Jerzy Świniarski on 29/07/16.
//  Copyright © 2016 JS. All rights reserved.
//

import UIKit

@IBDesignable class ViewController: UIViewController, JSImageSwitchButtonViewDelegate {

    @IBOutlet var myView1: JSImageSwitchButtonView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        myView1.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Delegate and notificaion methods
    func buttonPressed(sender: JSImageSwitchButtonView) {
        DLog("received")
    }
    func switchValueDidChange(sender: JSImageSwitchButtonView, newValue: Bool) {
        DLog("received: \(newValue)")
    }
}

