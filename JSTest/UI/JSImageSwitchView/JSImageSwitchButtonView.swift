//
//  JSImageSwitchButtonView.swift
//  JSTest
//
//  Created by Jerzy Świniarski on 29/07/16.
//  Copyright © 2016 JS. All rights reserved.
//

import UIKit

protocol JSImageSwitchButtonViewDelegate: class {
    func switchValueDidChange(sender: JSImageSwitchButtonView, newValue:Bool)
    func buttonPressed(sender: JSImageSwitchButtonView)
}

@IBDesignable class JSImageSwitchButtonView: UIView {

    weak var delegate:JSImageSwitchButtonViewDelegate?
    
    // Our custom view from the XIB file
    var view: UIView!
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var viewSwitch: UISwitch!
    @IBOutlet var button: UIButton!

    
    @IBInspectable var image: UIImage? {
        get {
            return imageView.image
        }
        set(image) {
            imageView.image = image
        }
    }
    
    @IBInspectable var titleText: String? {
        get {
            return titleLabel.text;
        }
        set(titleText) {
            titleLabel.text = titleText
        }
    }
    
    @IBInspectable var buttonBackgroundColor: UIColor? {
        get {
            return button.backgroundColor;
        }
        set(buttonBackgroundColor) {
            button.backgroundColor = buttonBackgroundColor
        }
    }
    
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "JSImageSwitchButtonView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    @IBAction func switchValueChanged(sender: AnyObject) {
        DLog("sending: \(self.viewSwitch.on)")
        delegate?.switchValueDidChange(self, newValue: self.viewSwitch.on)
    }
    
    @IBAction func buttonPressed(sender: AnyObject) {
        DLog("sending")
        delegate?.buttonPressed(self)
    }
}
